package pg_utils

import (
	"errors"
	"strings"

	"bitbucket.org/zamascodevelopmentteam/myabsen-utils-go/rest_errors"
	"github.com/lib/pq"
)

const (
	ErrorNoRows = "no rows in result set"
)

//ParseError for customize error pg
func ParseError(err error) rest_errors.RestErr {
	_, ok := err.(*pq.Error)
	if !ok {
		if strings.Contains(err.Error(), ErrorNoRows) {
			return rest_errors.NewNotFoundError("no record matching given id")
		}
		return rest_errors.NewInternalServerError("error parsing database response", err)
	}

	return rest_errors.NewInternalServerError("error processing request", errors.New("database error"))
}
